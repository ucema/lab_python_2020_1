# Buenas tardes, en unos instantes comenzamos
# Vayan pensando:
# Leer 20 numeros y determinar cuales son pares.

contador = 0
while contador < 20:
    numero = int(input('Ingrese un numero:'))
    if numero % 2 == 0:
        print('Es par')
    else:
        print('Es impar')

    contador = contador + 1

print('Fin')

