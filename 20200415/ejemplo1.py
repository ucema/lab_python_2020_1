
lista = range(10)   # Devuelve una lista con los numeros desde el 0 hasta el 9

for x in lista:
    print(x)

print('-' * 50)

for x in range(5):      # [0,1,2,3,4]
    print(x)

print('-' * 50)

for x in range(5, 10):  # 5,6,7,8,9
    print(x)

print('-' * 50)

for x in range(5, 10, 2):  # 5,7,9
    print(x)


print('-' * 50)

for x in range(100, 500, 12):
    print(x)

print('-' * 50)

for x in range(20, 10, -3):  
    print(x)
