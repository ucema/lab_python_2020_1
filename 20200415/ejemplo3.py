# Diccionarios: Clave - Valor

mi_diccionario = {1: 'uno', 2: 'dos', 3: 'tres'}
v = mi_diccionario[1]       # 1 es la clave en el diccionario
print(v)

otro = {'uno': 'one', 'dos': 'two', 'tres': 'three'}
v = otro['uno']
print(v)

diccionario1 = {}
diccionario1['clave1'] = 'valor 1'
diccionario1['clave2'] = 'valor 2'
diccionario1['clave3'] = 'valor 3'
print(diccionario1)
print(diccionario1['clave2'])

diccionario1['clave2'] = 'otro valor'
print(diccionario1['clave2'])

for clave in diccionario1:
    print(clave, '=', diccionario1[clave])

print('-' * 50)
for valor in diccionario1.items():
    print(valor)

print('-' * 50)
for clave in diccionario1.keys():
    print(clave)

print('-' * 50)
# diccionario1.clear()   # Deja el diccionario vacio
v = diccionario1.pop('clave2')
print(v)
print(diccionario1)

print('-' * 50)
# diccionario1.clear()   # Deja el diccionario vacio
v = diccionario1.popitem()  # remueve el ultimo elemento y devuelve la tupla
print(v)
print(diccionario1)

print(len(diccionario1))