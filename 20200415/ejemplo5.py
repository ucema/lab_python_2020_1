# Funciones

def sumar(lista):
    suma = 0
    for numero in lista:
        suma = suma + numero

    return suma

def sumar_listas(lista1, lista2):
    lista3 = []

    if len(lista1) > len(lista2):
        for i in range(len(lista2)):    # Range desde cero hasta la longitud de lista2 menos 1
            suma = lista1[i] + lista2[i]
            lista3.append(suma)
    else:
        for i in range(len(lista1)):    # Range desde cero hasta la longitud de lista1 menos 1
            suma = lista1[i] + lista2[i]
            lista3.append(suma)

    return lista3


lista1 = [5,6,8,9,10,9,3,12,52,48,96]

resultado = sumar(lista1)
print('Promedio:', resultado / len(lista1))

a = [45, 68, 30, 48 ,55]
b = [10, 65, 20, 9]
c = sumar_listas(a, b)
print(c)

