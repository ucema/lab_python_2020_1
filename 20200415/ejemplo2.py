# F(X) = X + 2  
# F(2) --->  4

def mostrar(x):  # Se define una funcion con un argumento llamado x
    print('---- Inicia funcion ----')
    
    for n in x:
        print(n)

    print('---- Finaliza funcion ----')



lista = range(20)
lista2 = ['uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho']

mostrar(lista)
mostrar(lista2)

a = lista2[3]    # Muestra el 4to elemento
print(a)

b = lista2[3:5]  # Muestra desde el 4to elemento hasta el 5to (No toma el 6to)
print(b)

c = lista2[0:4:2]
print(c)

d = lista2[:3]  # Es equivalente a lista2[0:3]
print(d)

e = lista2[3:]  # Es equivalente a lista2[3:tamaño lista]
print(e)

f = lista2[::-1]    # Invierte la lista
print(f)

g = lista2[::-2]
print(g)

print('-' * 50)
print(lista2)
lista2.pop(2)   # Elimina la 3ra posicion.
print(lista2)

print('-' * 50)
lista2.sort()    # Codigos ASCII: 65 es A    80 P    165 Ñ      64 @
print(lista2)

animales = ['pato', 'ñandu', 'abeja']
animales.sort()
print(animales)

lista2.reverse()
print(lista2)

