# Tuplas
t1 = ('a', 1, '20')
print(t1)

print(t1[2])

print(type(t1))

t2 = (2,)
print(type(t2))

print(len(t1))

for x in t1:
    print(x)

l1 = list(t1)   # Creo una lista a partir de la tupla
l1[1] = 'cambiado'
t1 = tuple(l1)  # Vuelvo a convertir la lista en tupla

print(t1)
