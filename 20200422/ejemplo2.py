def calcular_promedio(param1, *numeros):
    suma = 0
    print(numeros)
    for n in numeros:
        suma = suma + n

    promedio = suma / len(numeros)

    return promedio

def calcular_algo(**dicionario):
    print('*'*  50)
    for key, value in dicionario.items():
        print(key, value)

def hacer_mucho(param1, param2, *argv, **kwargs):
    print('Hago mucho')
    

prom1 = calcular_promedio(10, 5, 30, -9)
prom2 = calcular_promedio(-5, 5, 3)
print(prom1, prom2)

algo = calcular_algo(edad=20, nombre='Gabriel')
otro = calcular_algo(peso=12.5, signo='Geminis')