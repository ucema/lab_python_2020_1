# F(X) = X + 2  
# F(2) --->  4
# f(x,y) = 2x+y
# f(1,5) --> 7

# Cada variable tiene un scope o visibilidad
# Acoplamiento: se tiene que depende fuertemente de otro codigo

def leer_nombre(mensaje):
    print('*' * 50)
    nombre = input(mensaje)

    while nombre == '':
        print('Nombre invalido.')
        nombre = input(mensaje)

    return nombre

def leer_numero(desde, hasta, mensaje='Ingrese un numero:', otro=123):
    print(otro)
    print('*' * 50)
    numero = int(input(mensaje))
    while not (desde <= numero <= hasta):       # desde <= numero and numero <= hasta
        numero = int(input(mensaje))

    return numero

def leer_fecha():
    dia = int(input('Ingrese dia:'))
    mes = int(input('Ingrese mes:'))
    anio = int(input('Ingrese año:'))

    return dia, mes, anio


var1 = 'Ingrese un nombre:'

#x = leer_nombre(var1)
#y = leer_nombre(var1)
#print(x, y)

#a = leer_numero(0, 1000)
#a = leer_numero(0, 1000, otro=456)
#a = leer_numero(hasta=100, desde=20)

d, m, a = leer_fecha()      # Asignacion multiple
print(d,m,a)

x, y, z = -1, 2, 5
print(x,y,z)

# i, j = 10, 2*i
i = 10
j = 2*i
print(i, j)
