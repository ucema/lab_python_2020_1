# Proceso de abstraccion

# Lo objetos tiene:
# 1-Caracteristicas:
# patas
# Orejas
# Ojos
# Color de Pelo

# 2-Comportamiento:
# Maulla
# Corre
# Duerme
# Come

# 3-identificacion:
# Nombre o codigo genetico que lo hace único

# Clase: es una especie de PLANO codificada para crear OBJETOS (en Memoria)

class Gato():
    # Atributos (de la clase) ---> Las Caracteristicas del objeto
    color_pelo = ''
    edad = 0

    # Metodos (de la clase) ---> El Comportamiento de los objetos
    def correr(self):
        print('Gato corriendo...')

    def dormir(self, horas):
        print('Durmiendo ', horas, 'horas')


pipi = Gato()   # Se creó el objeto, y se "guardo" en pipi. Una Instancia de la clase Gato
pipi.correr()
pipi.dormir(8)






