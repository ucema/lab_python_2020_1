import operator

class Factura():
    numero = 0
    cantidades = []
    total = 0

    def __init__(self, numero=0, cantidades=[], total=0):
        self.cantidades = cantidades
        self.numero = numero
        self.total = total

    def __lt__(self, otro):
        return self.numero < otro.numero
    

def compararPorCantidades(factura):     # UNA FUNCION "SUELTA"
    suma = 0
    for c in factura.cantidades:
        suma += c

    return suma

f1 = Factura(1, [5,8,2], 1500)            # 15
f2 = Factura(2, [2,6,7,4], 1200)          # 19
f3 = Factura(3, [4,1], 1350)              # 5

facturas = [f1, f2, f3]
facturas.sort(key=compararPorCantidades)
for f in facturas:
    print(f.numero, '\t', f.cantidades, '\t', f.total)

print('*' * 50)
facturas.sort()
for f in facturas:
    print(f.numero, '\t', f.cantidades, '\t', f.total)

print('*' * 50)

facturas.sort(key=operator.attrgetter('total'))
for f in facturas:
    print(f.numero, '\t', f.cantidades, '\t', f.total)

print('*' * 50)
facturas.sort(reverse=True)
for f in facturas:
    print(f.numero, '\t', f.cantidades, '\t', f.total)
