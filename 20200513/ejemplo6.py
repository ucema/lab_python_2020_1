
lados = [3, 4, 2, 6, 7]

# superficies = []
# for l in lados:
#     superficies.append(l * l)

# def cuadrado(x):
#     return x * x

# superficies = list(map(cuadrado, lados))
# print(lados)
# print(superficies)

superficies = list(map(lambda x: x*x, lados))
print(lados)
print(superficies)

datos = list(zip(lados, superficies))
print(datos)

sin_sentido = list(map(lambda t: 'Lado:' + str(t[0]) + ', sup:' + str(t[1]), datos))
print(sin_sentido)


a = [1,2,3,4]
b = [5,6,7]
z = list(zip(a,b))
print(z)

a = [1,2,3]
b = [5,6,7]
c = [8,9,0]
z = list(zip(a,b,c))
print(z)

print('*' * 50)

numeros = [65,32,8766,232,54,67,87,342,756]
numeros_400 = list(filter(lambda x: x > 400, numeros))
numeros_pares = list(filter(lambda x: x % 2 == 0, numeros))

print(numeros)
print(numeros_400)
print(numeros_pares)

print('*' * 50)

numeros = [1,2,3,4,5]

resultado = 0
for elemento in numeros:
    resultado += elemento

print(resultado)

from functools import reduce

suma_nueva = reduce(lambda resultado, elemento: resultado + elemento, numeros)
print(suma_nueva)

nombres = ['uno', 'dos', 'tres', 'cuatro']
todos = reduce(lambda res, ele: res + '-' + ele, nombres)
print(todos)
