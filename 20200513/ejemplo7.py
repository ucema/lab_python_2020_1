nombres = ['uno', 'dos', 'tres', 'cuatro']

num = list(enumerate(nombres))
print(num)

num = list(enumerate(nombres, 10))
print(num)


valores = [True, False, True, True]
x = all(valores)
print(x)

x = any(valores)
print(x)