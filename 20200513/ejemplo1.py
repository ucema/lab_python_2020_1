class Persona():
    nombre = ''
    edad = 0
    altura = 0

    def __lt__(self, otro):         # Menor que  <
        return self.edad < otro.edad

    def __gt__(self, otro):         # Mayor que   >
        return self.edad > otro.edad

    def __le__(self, otro):         # Menor o igual que    <=
        return self.edad <= otro.edad

    def __ge__(self, otro):         # Mayor o igual que    >=
        return self.edad <= otro.edad

a = Persona()
a.nombre = 'uno'
a.edad = 2

b = Persona()
b.nombre = 'dos'
b.edad = 2

print(a<=b)
