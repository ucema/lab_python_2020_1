class Factura():
    numero = 0
    cantidades = []
    total = 0

    def __init__(self, numero=0, cantidades=[], total=0):
        self.cantidades = cantidades
        self.numero = numero
        self.total = total


    
f1 = Factura(1, [5,8,2], 1500)            # 15
f2 = Factura(2, [2,6,7,4], 1200)          # 19
f3 = Factura(3, [4,1], 1350)              # 5

facturas = [f1, f2, f3]
facturas.sort(key=lambda fac: fac.total)
for f in facturas:
    print(f.numero, '\t', f.cantidades, '\t', f.total)