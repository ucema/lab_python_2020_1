# Lambda

# f(x) = 2*x+3

f1 = lambda x : 2*x+3
print(f1(2))

f2 = lambda x,y : x*y
print(f2(2,6))

def ejemplo1(n):
    return lambda x : x * n

f3 = ejemplo1(10)
print(f3(3))            # 30

f4 = ejemplo1(3)
print(f4(2))
