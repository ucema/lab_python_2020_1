# JSON : JavaScript Object Notation
import json

s = ' { "nombre":"Gabriel", "apellido":"Barrera"  } '

o = json.loads(s)
print(o['nombre'])

d = {
    'nombre':'Gabriel', 
    'apellido':'Barrera'
}

y = json.dumps(d)
print('En JSON es: ' + y)



class Persona():
    nombre = ''
    edad = 0
    altura = 0

p = Persona()
p.nombre = 'Pipi'
p.edad = 20
p.altura = 175.3

y = json.dumps(p.__dict__)
print('En JSON es: ' + y)
print(type(y))