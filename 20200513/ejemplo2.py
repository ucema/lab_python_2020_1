a = [5,2,78,9,43,2,45]
a.sort()    
print(a)

b = ['uno', 'dos', 'cero', 'nuevo']
b.sort()
print(b)

class Persona():
    nombre = ''

    def __init__(self, nombre=''):
        self.nombre = nombre

    def __str__(self):
        return self.nombre

    def __lt__(self, otro):
        return self.nombre < otro.nombre


p = [Persona('uno'), Persona('dos'), Persona('cero'), Persona('nueve')]
for persona in p:
    print(persona)

print('*' * 50)
p.sort()
for persona in p:
    print(persona)

print('*' * 50)
p.reverse()
for persona in p:
    print(persona)
