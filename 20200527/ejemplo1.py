archivo1 = open('archivo1.txt', 'w')
archivo1.write('1 Lorem iptsum ......\n')
archivo1.write('2 Lorem iptsum ......\n')
archivo1.write('3 Lorem iptsum ......\n')
archivo1.write('4 Lorem iptsum ......\n')
archivo1.close()

archivo2 = open('archivo1.txt')   # 'rt'
contenido = archivo2.read() # Lee todo el contenido
print(contenido)
archivo2.close()

print('*' * 50)
archivo2 = open('archivo1.txt')
for linea in archivo2:    # Lee en forma secuencial, linea por linea
  # print(linea.replace('\n', ''))
  print(linea, end='')

archivo2.close()

print('*' * 50)
archivo2 = open('archivo1.txt')
lineas = archivo2.readlines()   # Lee el archivo y devuelve una lista con las lineas
print(lineas)
archivo2.close()

print('*' * 50)
archivo2 = open('archivo1.txt')
linea = archivo2.readline(10)   # Lee el archivo y devuelve una lista con las lineas
print(linea)
archivo2.close()


# archivo1 = open('archivo2.txt', 'x')
# archivo1.write('Que pasa con esta linea\n')
# archivo1.close()

archivo1 = open('archivo3.txt', 'a')
archivo1.write('5 Lorem iptsum ......\n')
archivo1.write('6 Lorem iptsum ......\n')
archivo1.write('7 Lorem iptsum ......\n')
archivo1.write('8 Lorem iptsum ......\n')
archivo1.close()


print('Fin del programa')

'''
w: Crear o abrir para escribir (Sobreescribe)
r: Abrir para leer (Default)
x: Crear un NUEVO archivo
a: Agregar al final del archivo si existe, o crear uno nuevo caso q no exista

t: Texto  (default)
b: Binario
'''