def abrir():
  archivo1 = open('archivo2.txt', 'x')
  archivo1.write('Que pasa con esta linea\n')
  archivo1.close()

def dos():
  x, y = 1, 0
  z = x / y
  return z

def uno():
  return dos()

try:
  print('*****************')
  z = uno()
  print('----------------')
  print(z)
except:
  print('Hubo un error')


print('/' * 50)

try:
  z = uno()
  abrir()
except ZeroDivisionError:
  print('por cero no se puede')
except ArithmeticError:
  print('error aritmetico')
except FileExistsError:
  print('El archivo ya existe')
except:
  print('no se que paso')





print('Fin feliz')



# FileExistsError
# ZeroDivisionError

# Exception