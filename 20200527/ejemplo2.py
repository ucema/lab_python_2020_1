# CSV: Comma Separated Value

a = open('ventas.csv')

suma_total = 0

for linea in a:
  campos = linea.split(',')
  
  cuit = campos[0]
  punto_venta = int(campos[1])
  numero = int(campos[2])
  total = float(campos[3])

  suma_total += total
  
  print(cuit, punto_venta, numero, total)

print(suma_total)
a.close()

