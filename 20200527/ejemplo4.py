class MiError(Exception):
  
  def __init__(self, param1, param2):
    self.param1 = param1
    self.param2 = param2


def leer_entero_positivo():
  n = int(input('Ingrese:'))

  if n < 0:
    raise MiError('Ingresaste mal el dato.', 12345)

  return n


try:
  numero = leer_entero_positivo()
except MiError as e:
  print('Esta mal ingresado')
  print(e.param1)
  print(e.param2)