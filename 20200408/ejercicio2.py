# Leer 3 numeros, sumar los 2 primeros, y si el tercero es distinto de cero, 
# realizar la division de la suma sobre el tercero.

numero1 = int(input('Ingrese numero:'))
numero2 = int(input('Ingrese numero:'))
numero3 = int(input('Ingrese numero:'))

suma = numero1 + numero2

if numero3 != 0:
    division = suma // numero3
    print(division)
else:
    print('El tercer numero es cero')

