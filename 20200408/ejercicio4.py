# Leer numeros mientras sean mayores a cero y
# menores a 1000
# Al final mostrar la suma de los pares y la 
# suma de los impares.

suma_pares = 0
suma_impares = 0

numero = int(input('Ingrese un numero: '))

while numero > 0 and numero < 1000:
    if numero % 2 == 0:
        suma_pares = suma_pares + numero
    else:
        suma_impares = suma_impares + numero

    numero = int(input('Ingrese un numero: '))

print('La suma de pares es', suma_pares)
print('La suma de impares es', suma_impares)

