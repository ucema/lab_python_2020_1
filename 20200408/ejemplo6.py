otra_lista = ['gato', 'perro', 'canario', 'pez globo']

# para cada animal en otra_lista
for animal in otra_lista:
    print(animal) 

mi_lista = [20, 10, 3, 50, 99, 13]
suma = 0

for numero in mi_lista:
    print('sumando', numero)
    suma = suma + numero

print(suma)