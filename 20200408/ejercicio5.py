# Mostrar las tablas de multiplicar 
# 2 x 1 = 2
# 2 x 2 = 4
# ...
# 10 x 10 = 100

factor = 2
while factor <= 10:
    numero = 1
    while numero <= 10:
        producto = factor * numero
        print(factor, '*', numero, '=', producto)

        numero = numero + 1
        
    print('-' * 50)

    factor = factor + 1