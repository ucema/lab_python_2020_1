# Pedir el numero de mes, y mostrar cuantos dias 
# tiene (suponer febrero 29)

mes = int(input('Ingrese mes:'))

# 1,3,5,7,8,10,12 tienen 31
# 2 tiene 29
# el resto 30

if mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12:
    print('Tiene 31 dias')
elif mes == 2:
    print('Tiene 29 dias')
else:
    print('Tiene 30 dias')
    

