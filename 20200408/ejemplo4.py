# Listas: conjunto de datos (tipicamente del mismo tipo)

mi_lista = [20, 10, 3, 50, 99, 13]          # vectores o arrays
otra_lista = ['gato', 'perro', 'canario', 'pez globo']

print('El primer elemento:', mi_lista[0])
print('El primer elemento:', otra_lista[0])

mi_lista[0] = 35

print('El primer elemento:', mi_lista[0])

print('La lista tiene', len(mi_lista), 'elementos.')

i = 0
while i < len(mi_lista):
    print(mi_lista[i])

    i = i + 1
