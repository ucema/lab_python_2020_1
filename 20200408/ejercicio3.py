# Leer numeros mientras sean mayores a cero, y al final mostrar
# la suma.

suma = 0

# Inicializar las variables de la condicion del while
numero = int(input('Ingrese un numero: '))

while numero > 0:
    # Proceso repetitivo
    suma = suma + numero

    # AL FINAL DEL WHILE: La actualizacion de las variables en la condicion
    numero = int(input('Ingrese un numero: '))


print('La suma es', suma)
