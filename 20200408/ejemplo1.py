numero1 = int(input('Ingrese numero:'))

if numero1 % 5 == 0:
    print('Es divisible por 5')
elif numero1 % 4 == 0:                      # Es la union de else e if
    print('Es divisible por 4')
elif numero1 % 3 == 0:
    print('Es divisible por 3')
elif numero1 % 2 == 0:
    print('Es divisible por 2')

print('Fin.')