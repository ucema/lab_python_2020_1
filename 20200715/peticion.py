import requests, json

resp = requests.get('http://127.0.0.1:8080/j')

if resp.status_code == 200:
    # print(resp.headers)
    o = resp.json()
    print(o['campo2'])

print('*' * 50)
resp = requests.get('https://httpbin.org/get?param1=1234&parma2=abcd')
if resp.ok:
    j = resp.json()
    print(j)
    print('*' * 50)
    print(j['args']['param1'])

print('*' * 50)

auto = {'patente': 'XXX555', 'modelo':'super', 'peso': '1200'}
resp = requests.post('https://httpbin.org/post', data=auto)
print(resp.json())


