import numpy as np

a = np.array( [3, 5, 10, 4] )
b = np.array( [4, 2, 1, 6] )

suma = a + b
print(suma)

resta = a - b
print(resta)

x = 2*a
print(x)

m = a * b
print(m)

m1 = np.array( [[4,1], [8,2]])
m2 = np.array( [[0,1], [3,5]])
print(m1+m2)
print(m1 * m2)

print('-'*20)
ran = np.random.default_rng(0)
b = ran.random((5,5))
print(b)
c = ran.random((5,5))
print(c)

print()
m = 10 * c
print(m)
m1 = m.astype('int32')    # Cambiar el tipo de dato
print(m1)

print(m1.sum())
print(m1.min())
print(m1.max())

suma = m1.sum(axis=0) # Sumarizo por columnas
print(suma, type(suma))

suma = m1.sum(axis=1) # Sumarizo por filas
print(suma)

print('-' * 50)
e = np.arange(5)
print(e)
f_e = np.exp(e)       # funcion exponecial e^x
print(f_e)

f_r = np.sqrt(e)
print(f_r)

print('-' * 50)

print('Std:', m1.std())
print('Promedio:', m1.mean())
print('Mediana:', np.median(m1))
print(m1.transpose())
print(np.invert(m1))
