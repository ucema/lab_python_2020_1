import numpy as np

ran = np.random.default_rng(0)
a = ran.random((10))
a = a * 100
a = a.astype('int32')
print(a)

print(a[2:8])       # Slice
print(a[2:8:2])
print(a[::-1])
print(a[2::-1])

b = np.arange(10) ** 3 
print(b)


def f(x, y):
    return 5*x+2*y

print(f(1,1))
mf = np.fromfunction(f, (3,6), dtype=int)
print(mf)
print(mf.shape)   # Devuelve la "forma" de la matriz:   (3, 6)

print('-' * 50)

for fila in mf:
    print(fila)

for valor in mf.flat:
    print(valor)
