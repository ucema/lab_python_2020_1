import numpy as np

vec1 = np.array([1,2,3,4])
print(vec1)
print(vec1[0])

vec2 = np.arange(1, 100, 2)
print(vec2)
print(type(vec2))

# ndarray: n-dim array

print(vec2.ndim)  # Dimension del array

mat1 = vec2.reshape(5, 10)  # reformatea el vector a una matriz
print(mat1)
print(mat1.ndim)

vec3 = np.arange(27)
mat2 = vec3.reshape(3, 3, 3)
print(mat2)
print('Dimension:', mat2.ndim)
print('Items: ', mat2.size)

print(mat2.data)  # buffer directamente a la memoria de los datos de la matriz

print(mat2.dtype.name)

vec4 = np.array([1,2,3,4], dtype=float)
print(vec4)

vec5 = np.array([1, 2, 3, 4], dtype=complex)
print(vec5)

ceros = np.zeros((3,6), dtype=int)
print(ceros)

vector_ceros = np.zeros((10,))
print(vector_ceros)

unos = np.ones((2,3,3), dtype=int)
print(unos)

sin_inicializar = np.empty((3,4))
print(sin_inicializar)

vec6 = np.arange(-5.2, 5, 0.5)
print(vec6)

vec7 = np.linspace(1, 10, 19)
print(vec7)

x = np.linspace(0, 2 * np.pi, 100)
f_seno = np.sin(x)
print(f_seno)

