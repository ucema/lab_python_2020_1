# DataFrames

import pandas as pd

datos = {"pais": ['Argentina', 'Uruguay', 'Chile', 'Brasil', 'Peru'],
         "capital": ['CABA', 'Montevideo', 'Santiago', 'Brasilia', 'Lima'],
         "poblacion": [44, 12, 23, 120, 32],
         "superficie": [200, 10, 45, 320, 160] }

df = pd.DataFrame(datos)
print(df)
print()

df.index = ['AR', 'UR', 'CH', 'BR', 'PE']
print(df)

print('*' * 50)

datos_csv = pd.read_csv('paises.csv')
print(datos_csv)

print('*' * 50)

# datos_clip = pd.read_clipboard()
# print(datos_clip)

df2 = pd.DataFrame(datos, index=['AR', 'UR', 'CH', 'BR', 'PE'])
print(df2)

print('*' * 50)

datos_csv = pd.read_csv('paises_index.csv', index_col=0)
print(datos_csv)

print('*' * 50)

print(df2[['pais','poblacion']])

print('*' * 50)

print(df2.head(3))

print('*' * 50)

print(df2.tail(3))

print('*' * 50)

print(df2.describe())

print('*' * 50)

print(df2.sort_index(ascending=False))

print('*' * 50)

print(df2.sort_values(by='superficie',ascending=False))