class Persona():
    nombre = ''
    edad = 0
    altura = 0

    def __eq__(self, otro):
        if not isinstance(otro, Persona):
            return NotImplemented               # Producir un error personalizado

        return self.nombre == otro.nombre

    def __ne__():
        return self.nombre != otro.nombre


class Auto():
    patente = ''
    nombre = ''


v = Auto()
v.patente = 'XXX555'
v.nombre = 'Nombre 1'


a = Persona()
a.nombre = 'Nombre 1'
a.edad = 35
a.altura = 162

b = Persona()
b.nombre = 'Nombre 1'
b.edad = 35
b.altura = 174

if a == b:      # a.__eq__(b)
    print('Son iguales')
else:
    print('Son distintos')

if a == v:
    print('Son iguales')
else:
    print('Son distintos')