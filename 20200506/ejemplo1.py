

'''
DNI = 98
CUIT = 96
CUIL = 95
PASAPORTE = 10
'''

from enum import Enum

class TipoDocumento(Enum):
    dni = 98
    cuit = 96
    cuil = 95
    pasaporte = 10

    def __str__(self):
        return 'El Tipo de documento es ' + self.name + ' y el codigo es ' + str(self.value)


class Cliente():
    razon_social = ''
    tipo_documento = TipoDocumento.dni
    numero_documento = 0
    domicilio = ''

print(TipoDocumento(98))
print(TipoDocumento.dni)

print ('*' * 50)
for td in TipoDocumento:
    print(td)

a = TipoDocumento.dni
b = TipoDocumento.dni

print(a is b)
print(a is not b)
print(a == b)
print(a != b)

print ('*' * 50)
c = int(input('Ingrese:'))
d = TipoDocumento(c)
print(d)
