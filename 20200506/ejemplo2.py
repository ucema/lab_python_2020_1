
class Carnivoro():

    def metodo1(self):
        print('Soy Carnivoro')

    def metodoX(self):
        print('Hago X de Carnivoro')


class Mamifero():

    def metodo2(self):
        print('Soy Mamifero')

    def metodoX(self):
        print('Hago X de Mamifero')


class Lobo(Mamifero, Carnivoro):    # Herencia Multiple

    def metodoSub1(self):
        print('Soy SubClase')


s = Lobo()
s.metodo1()         # Heredado de Carnivoro
s.metodo2()         # Heredado de Mamifero
s.metodoX()         # se ejecuta el metodo del primer "padre" al momento de definir la subclase
s.metodoSub1()

# El problema del Diamante