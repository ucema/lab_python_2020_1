class Articulo():   # Object __str__()
    codigo = ''
    descripcion = ''
    precio = 0

    def __init__(self, codigo='', descripcion='', precio=0):
        self.codigo = codigo
        self.descripcion = descripcion
        self.precio = precio

    def __str__(self):
        s = 'Codigo: ' + self.codigo
        s = s + '\tDescripcion: ' + self.descripcion
        s = s + '\tPrecio: ' + str(self.precio)                  # Observar

        return s


a = Articulo('UNO', 'Desc UNo', 1)
print(a)

a.precio = 2.4
print(a)

for i in range(5):
    nombre = input('Campo a modificar:')
    valor = input('El valor:')

    a.__setattr__(nombre, valor)
    print(a)

nombre = input('Campo a consultar:')
valor = a.__getattribute__(nombre)
print(valor)

print(a.codigo)

print(type(valor))

# WARNING!!!!
# Se le agrega al OBJETO el atributo (NO a la claes)
a.sarasa = 'Soy SARASA'

print(a.sarasa)
