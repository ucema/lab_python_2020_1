'''
Crear una clase que represente un articulo que se vende.
Crear una clase que represente una factura de venta.
'''
class Articulo():   # Object __str__()
    codigo = ''
    descripcion = ''
    precio = 0

    def __init__(self, codigo='', descripcion='', precio=0):
        self.codigo = codigo
        self.descripcion = descripcion
        self.precio = precio

    def __str__(self):
        s = 'Codigo: ' + self.codigo
        s = s + '\tDescripcion: ' + self.descripcion
        s = s + '\tPrecio: ' + str(self.precio)                  # Observar

        return s


class Cliente():
    razon_social = ''
    tipo_documento = ''
    numero_documento = 0
    domicilio = ''

    # ToDo: __init__ y __str__


class ItemFactura():
    articulo = None
    cantidad = 0

    def __init__(self, articulo, cantidad=1):
        self.articulo = articulo
        self.cantidad = cantidad


    # ToDo: __init__ y __str__


class FacturaVenta():
    letra = ''
    fecha_emision = ''
    punto_venta = 0
    numero = 0

    iva = 0
    neto = 0

    cliente = None

    items = []
    

a1 = Articulo('JTM', 'Jugo Tang Manzana', 10.15)
a2 = Articulo('A2', 'Artic. Dos', 2.1)

i1 = ItemFactura(a1, 5)
i2 = ItemFactura(a2)

c = Cliente()
c.razon_social = 'Pirulo Customer'
c.tipo_documento = 99
c.numero_documento = 35000001

f = FacturaVenta()
f.cliente = c
f.items.append(i1)
f.items.append(i2)


print(a1)
print(a2)

