
class Vehiculo():
    marca = ''
    color = ''
    patente = ''

    def __init__(self, patente):
        print('Constructor de Vehiculo')
        self.patente = patente

    def andar(self):
        print('andando...')


class Automovil(Vehiculo):      # La clase Automovil hereda de Vehiculo
    cantidad_asientos = 0
    
    def __init__(self, patente, cantidad_asientos=4):
        print('Constructor de Automovil')
        super(Automovil, self).__init__(patente)        # Invocacion al constructor de la super clase o clase padre
        self.cantidad_asientos = cantidad_asientos


class Sport(Automovil):
    pass



class Moto(Vehiculo):
    cilindrada = 250


class Camion(Vehiculo):
    tara = 2500

    def cargar(self):
        print('Camion cargando')


a = Automovil('XXX555', 6)
a.andar()
print(a.patente)
print(a.cantidad_asientos)

# c = Camion()
# c.andar()
# c.cargar()