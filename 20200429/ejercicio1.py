'''
1) Realizar una funcion que devuelva numeros pares. Para ello le debe ir pidiendo numeros
al usuario, y en la medida que sean pares los guarde en una lista que será
la que devolverá la funcion. Se ingresaran hasta que el usuario ingrese un 0.
Ejemplificar su uso.

2) Extender la funcion anterior, para que devuelva una lista de multiplos de un numero dado por 
parametro, si no se le pasa un parametros debe devolver pares.
'''

# ACOPLAMIENTO



def leer_numeros(multiplo=2):
    numeros = []

    numero = int(input('Ingrese un numero: '))      # float() --> numeros reales
    while numero != 0:

        if numero % multiplo == 0:
            numeros.append(numero)

        numero = int(input('Ingrese un numero: '))

    return numeros

x = leer_numeros(4)
print(x)