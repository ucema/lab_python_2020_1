
class Gato():
    # Atributos (de la clase) ---> Las Caracteristicas del objeto
    color_pelo = ''
    edad = 0

    # Metodos (de la clase) ---> El Comportamiento de los objetos
    def correr(self):
        print('Gato corriendo...')

    def dormir(self, horas):
        print('Durmiendo ', horas, 'horas')

    def __str__(self):  # Es el metodo que se invoca para representar el objeto como texto/string
        return 'Soy gato de color ' + self.color_pelo


pipi = Gato()   # Se creó el objeto, y se "guardo" en pipi. Una Instancia de la clase Gato
pipi.color_pelo = 'Negro'
pipi.edad = 8

tito = Gato()
tito.color_pelo = 'Blanco'
tito.edad = 7

print(pipi)
print(tito)
