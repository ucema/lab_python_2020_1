# Operadores relacionales
# >     ==      <       >=      <=      !=
numero1 = int(input('Ingrese numero:'))
numero2 = int(input('Ingrese numero:'))

print(numero1 > numero2)

# if  expresion boolean o logica

if numero1 > numero2:
    print('estoy en el if')
    print('sigo en el if')
    print('aun estoy en el if')

print('fin, por ahora')

print('-' * 40)

if numero1 > numero2:
    print('El numero1 es mayor')
else:
    print('No es mayor')

print('Fin')