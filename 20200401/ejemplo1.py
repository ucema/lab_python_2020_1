print('Hola Mundo')
print('Esto es python')
print('Comenzando...')

# Esto es un comentario
# Ahora defino 2 variables
a = 123
b = 456

suma = a + b
# Operadores aritmeticos
# +  Sumar
# -  Restar
# *  Producto
# // Division Entera
# /  Division Real
# %  Modulo o resto de la division entera


# Operadores logicos
# and  Y o conjuncion
# or   O o disyuncion
# not  No o negacion
# Los valores logicos son: True / False

p = True
q = False

s = p and q

print('Valor de s =', s, ' fin ')
print('Negacion de p =', not p)
print('P o Q =', p or q)

# Enteros, reales,         logicos y Textos (o alfanumericos): Tipos de datos
# int      float/double    boolean   String

t = '123'   # Es un string
u = 3
o = t * u

print(o)

t = 555
print(t * 2)

# Nomenclatura, o la forma de escribir el codigo
# Variables: todo en minusculas y reemplazar el espacio por _
importe_haber = 0
importe_debe = 0

lectura_desde_teclado = input('Ingrese algo: ')
# el input "siempre" devuelve texto o string

print('Usted ingreso', lectura_desde_teclado)

numero = int(input('Ingrese un numero: '))

print('El doble es:', numero * 2)

un_numero_real = float(input('Ingrese un real: '))
print('El doble es:', un_numero_real * 2)
