from flask import Flask, request

# 127.0.0.1 --- localhost
# :5000 es el puerto

app = Flask(__name__)

@app.route('/')       # Al definir la ruta, tambien se define el metodo http, por default GET
def raiz_sitio():
    return 'Hola'

@app.route('/', methods=['POST', 'PUT'])
def peticion_post():
    return 'desde un post'

@app.route('/search')
def busqueda_google():
    print(request.args)
    print(request.remote_addr)
    return 'buscando...'

@app.route('/productos/<codigo>/<familia>')
def leer_producto(codigo, familia):
    print('Piden el producto ', codigo, familia)
    return 'producto ' + codigo

@app.route('/productos', methods=['POST'])
def alta_producto():
    print(request.form)

    if not 'apellido' in request.form:
        return 'falta apellido'

    for key in request.form:
        print(key, '=', request.form[key])
        
    return 'se ingresa'


if __name__ == '__main__':
    app.run(port=8080)

'''
 URL: https://www.google.com/search?client=firefox-b-d&q=yahoo
  https:  Protocolo de transmision
  www.google.com: dominio
  /search:   recursos
  ?client=firefox-b-d&q=yahoo  : son parametros
    & : separador de argumentos o parametros

  
'''
