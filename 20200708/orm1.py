from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Float, Integer

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


Base = declarative_base()


class Producto(Base):
    __tablename__ = 'productos'
    id = Column(Integer, primary_key=True)
    descripcion = Column(String)
    precio = Column(Float)
    peso = Column(Float)
    proveedor = Column(Integer)

    def __str__(self):
        return self.descripcion

engine = create_engine('sqlite:///mibase.sqlite')      # Connection String

Base.metadata.create_all(engine)  # Crear el modelo en la base de datos.

Session = sessionmaker()
Session.configure(bind=engine)


# Grabar un producto

a = Producto()
a.descripcion = 'teclado'
a.precio = 1253.85
a.peso = 650
a.proveedor = ''


s = Session()
s.add(a)
s.commit()
print('Se grabo con id', a.id)


s = Session()
prds = s.query(Producto)
for p in prds:
    print(p.id, p.descripcion, p.precio)

print('******')

prds = s.query(Producto).filter(Producto.id==2)
for p in prds:
    print(p.id, p.descripcion, p.precio)


# ORM  Object Relational Mapping
# Base de datos relacional: Tablas (Access, SQL Server, MySQL, Oracle, Maria DB, etc.)
# SQL: lenguaje de programacion de base de datos

# insert into productos (descripcion, precio, peso, proveedor) values ('mouse', 350.45, 120, '')

