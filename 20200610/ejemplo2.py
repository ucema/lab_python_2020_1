import numpy as np

ran = np.random.default_rng(0)

a = np.floor(ran.random((2,3)) * 100)
b = np.floor(ran.random((2,4)) * 100)
c = np.floor(ran.random((2,2)) * 100)

print(a)
print(b)
print(c)

print('-' * 50)

dh = np.hstack((a, b, c))   # Recibe una tupla de matrices, con las matrices a apilar en forma horizontal
                        # en este caso, la cantidad de FILAS debe ser igual en las matrices.
print (dh)

print('-' * 50)

a = np.floor(ran.random((2,3)) * 100)
b = np.floor(ran.random((3,3)) * 100)
c = np.floor(ran.random((1,3)) * 100)

print(a)
print(b)
print(c)

print('-' * 50)

dv = np.vstack((a,b,c))
print(dv)

print('-' * 50)

s = np.hsplit(dv.reshape(3,6), 2)
print(dv.reshape(3,6))
print(s[0])
print(s[1])

print('-' * 50)

vs = np.vsplit(dv, 3)   # dv tiene 6 filas. 1,2,3,6
print(vs[0])
print(vs[1])
print(vs[2])