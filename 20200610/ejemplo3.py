import numpy as np

ran = np.random.default_rng(0)
a = np.floor(ran.random((5,6)) * 100)
print(a)

print('-' * 50)

i = np.identity(6)
print(i)

print('-' * 50)

d = np.argwhere( a == 81 )
print(d)

print('-' * 50)

print(a.argmax())
print(a.ravel()[a.argmax()])

print(a.argmin())

print('-' * 50)

c = a.view()
print(a[0,0])
a[0,0] = -1
print(a[0,0])
print(c[0,0])
c[0,0] = -2
print(a[0,0])
print(c[0,0])

print('-' * 50)

d = a.copy()
print(a[0,0])
print(d[0,0])
d[0,0] = 1000
print(a[0,0])
print(d[0,0])


print('-' * 50)
print(a)
suma = np.sum(a, axis=0)
print(suma)

print('-' * 50)
prod = np.prod(a, axis=0)
print(prod)