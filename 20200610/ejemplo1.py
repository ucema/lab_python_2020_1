import numpy as np

ran = np.random.default_rng(0)
a = ran.random((5,6)) * 100
print(a)

print('-' * 50)

b = np.floor(a)     # Funcion que trunca el valor
print(b)

print('-' * 50)

c = np.ceil(a)
print(c)

print('-' * 50)

b = a.ravel()       # Devuelve una copia de los datos en un ndarray de 1 dimension
                    # a diferencia de .flat que devuelve un "iterador"
print(b)

print('-' * 50)

a1 = b.reshape(15,2)  # Cambia las dimensiones, hay que tener cuidado que las dimensiones
                      # tenga la misma cantidad de elementos, 3x10 = 5x6
print(a1)

print('-' * 50)

a2 = b.reshape(6, -1) # Si algunas de las dimensiones es -1, las otras se calculan automaticamente
print(a2)

print('-' * 50)

b.resize(2,15)        # La diferencia con reshape, es que reshape devuelve un nuevo ndarray, 
                      # en cambio resize, modifica el ndarray
print(b)

print('-' * 50)

transpuesta = a1.T    # Un version abreviada de obtener la transpuesta de una matriz
print(transpuesta)
print(transpuesta.shape)